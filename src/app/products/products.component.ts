import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../services/admin.service';
import { AuthService } from '../services/auth.service';
import { BackendService } from '../services/backend.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  list: any[] = [];

  constructor(private backendService: BackendService, private adminService: AdminService, private router: Router, public authService: AuthService) { }

  ngOnInit(): void {
    this.backendService.getProducts().subscribe(res => {
      this.list = res;
    });
  }

  add(product: any): void {
    this.authService.loggedIn.subscribe(res => {
      if (res) {
        this.backendService.add(product).subscribe(res => {
          console.log(res);
        }); 
      } else {
        this.router.navigate(['/login']);
      }
    });
  }

  deleteProduct(id: any): void {
    this.adminService.deleteProduct(id).subscribe(res => {
      console.log(res);
      if (res.success) {
        this.ngOnInit();
      }
    })
  }

}
