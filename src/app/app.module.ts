import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyCartComponent } from './my-cart/my-cart.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { FormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { AppGuard } from './app.guard';
import { ProductsComponent } from './products/products.component';
import { BackendService } from './services/backend.service';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { AdminComponent } from './admin/admin.component';
import { AdminService } from './services/admin.service';


@NgModule({
  declarations: [
    AppComponent,
    MyCartComponent,
    RegisterComponent,
    LoginComponent,
    LogoutComponent,
    MyOrdersComponent,
    ProductsComponent,
    UserDetailComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    AuthService,
    BackendService,
    AdminService,
    AppGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
