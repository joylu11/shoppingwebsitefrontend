//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { BackendService } from '../services/backend.service';
import { AuthService } from '../services/auth.service';
import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})
export class MyOrdersComponent implements OnInit {
  list;
  userDetail;

  constructor(private backendService: BackendService, public authService: AuthService, private adminService: AdminService) { }

  ngOnInit(): void {
    if (this.authService.isAdmin) {
      this.adminService.getAllOrders().subscribe(res => {
        this.list = res;
        for (let i = 0; i < this.list.length; i++) {
          let id = res[i].id;
          let totalPrice = res[i].totalPrice;
          let products = res[i].products;
          this.adminService.getUserDetailByOrderId(this.list[i].id).subscribe(res2 => {
            this.list[i] = {
              'id': id,
              'totalPrice': totalPrice,
              'products': products,
              'fullName': res2.fullName,
              'phone': res2.phone,
              'address': res2.address,
              'city': res2.city,
              'state': res2.state,
              'zip': res2.zip
            }
          });
        }
        console.log(this.list);  
      });
    } else {
      this.backendService.getMyOrders().subscribe(res => {
        this.list = res;
        // quatity
      });
      this.backendService.getUserDetail().subscribe(res => {
        this.userDetail = res;
      });
    }
  }

}
