//@ts-nocheck
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppConfig } from './app.config';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  private API_URL = AppConfig.API_URL;

  constructor(private http: HttpClient, private authService: AuthService, private router: Router) { }

  getProducts(): Observable<any> {
    return this.http.get(this.API_URL + "/main")
      .pipe(map(res => {    
        return res;
    }));
  }

  getMyCart(): Observable<any> {
    return this.http.get(this.API_URL + "/cart/my/" + this.authService.userId)
      .pipe(map(res => {
        return res;
    }));
  }

  add(product: any): Observable<any> {
    return this.http.post(this.API_URL + "/cart/" + this.authService.userId, product)
      .pipe(map(res => {
        return res;
    }));
  }

  delete(product: any): Observable<any> {
    return this.http.delete(this.API_URL + "/cart/" + this.authService.userId, { body: product })
      .pipe(map(res => {
        return res;
    }));
  }

  checkout(myCart: any): Observable<any> {
    return this.http.post(this.API_URL + "/order", myCart)
      .pipe(map(res => {
        return res;
    }));
  }

  userDetail(userDetail: any): Observable<any> {
    return this.http.post(this.API_URL + "/userDetail/" + this.authService.userId, userDetail)
      .pipe(map(res => {
        return res;
    }));
  }

  getMyOrders(): Observable<any> {
    return this.http.get(this.API_URL + "/order/my/" + this.authService.userId)
      .pipe(map(res => {
        return res;
    }));
  }

  getUserDetail(): Observable<any> {
    return this.http.get(this.API_URL + "/userDetail/" + this.authService.userId)
      .pipe(map(res => {
        return res;
    }));
  }



}
