import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppConfig } from './app.config';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private API_URL = AppConfig.API_URL;

  constructor(private http: HttpClient) { }

  addProduct(product: any): Observable<any> {
    return this.http.post(this.API_URL + "/main/add", product)
      .pipe(map(res => {
        return res;
    }));
  }

  deleteProduct(id: any): Observable<any> {
    return this.http.delete(this.API_URL + "/main/delete/" + id)
      .pipe(map(res => {
        return res;
    }));
  }

  getAllOrders(): Observable<any> {
    return this.http.get(this.API_URL + "/order")
      .pipe(map(res => {
        return res;
    }));
  }

  getUserDetailByOrderId(id: any): Observable<any> {
    return this.http.get(this.API_URL + "/userDetail/admin/" + id)
      .pipe(map(res => {
        return res;
    }));
  }
  
}
