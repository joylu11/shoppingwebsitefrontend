// @ts-nocheck
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppConfig } from './app.config';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private API_URL = AppConfig.API_URL;

  loggedIn: Subject<boolean> = new BehaviorSubject<boolean>(false);
  userId: int;
  isAdmin: boolean;

  constructor(private http: HttpClient, private router: Router) { }

  register(user: any): Observable<any> {
    return this.http.post(this.API_URL + "/users", user)
      .pipe(map(res => {
        console.log(res);
        if (res.success) {
          this.router.navigate(['/login']);
        }
    }));
  }

  login(user: any): Observable<any> {
    let params = new HttpParams();
    params = params.append("username", user.username);
    params = params.append("password", user.password);
    // { withCredentials: true } browser set-cookie
    return this.http.post(this.API_URL + "/login", params)
      .pipe(map(res => {
        this.loggedIn.next(res.success);
        if (this.loggedIn) {
          this.getUserId(user.username).subscribe(res => {
            this.userId = res;
            this.checkIsAdmin(this.userId).subscribe(res => {
              this.isAdmin = res.success;
              console.log(this.isAdmin);
            });
          });
          this.router.navigate(['/']);
        }
        return res;
      }));
  }

  checkIsAdmin(id: int): Observable<any> {
    return this.http.get(this.API_URL + "/users/isAdmin/" + id)
      .pipe(map(res => {
        return res;
    }));
  }

  getUserId(username): Observable<any> {
    return this.http.get(this.API_URL + "/users/name/" + username)
      .pipe(map(res => {
        return res;
    }))
  }

  checklogin(): Observable<any> {
    return this.http.get(this.API_URL + "/checklogin", { withCredentials: true })
      .pipe(map(res => {
        this.loggedIn.next(res.success);
        return res;
      }));
  }

  logout(): Observable<any> {
    return this.http.post(this.API_URL + "/logout", { withCredentials: true })
      .pipe(map(res => {
        this.loggedIn.next(false);
        this.isAdmin = false;
        this.router.navigate(['/login']);
        return res;
      }));
  }
  
}
