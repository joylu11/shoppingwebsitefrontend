import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { BackendService } from '../services/backend.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.authService.checklogin().subscribe(res => {
      if (res.success) {
        this.router.navigate(['/main']);
      }
    });
  }

  login(user: any): void {
    this.authService.login(user).subscribe(res => {
      
    });
  }

}
