import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../services/admin.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private adminService: AdminService, private router: Router) { }

  ngOnInit(): void {
  }

  addProduct(product: any): void {
    this.adminService.addProduct(product).subscribe(res => {
      console.log(product);
      console.log(res);
      if (res.success) {
        this.router.navigate(['/main']);

      }

    });
  }

}
