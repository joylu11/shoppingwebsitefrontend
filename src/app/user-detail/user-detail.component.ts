import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendService } from '../services/backend.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  constructor(private backendService: BackendService, private router: Router) { }

  ngOnInit(): void {
  }

  userDetail(userDetail: any): void {
    console.log(userDetail);
    this.backendService.userDetail(userDetail).subscribe((res: { success: any; }) => {
      if (res.success) {
        this.router.navigate(['/mycart']);
      }
    });
  }

}
