//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { BackendService } from '../services/backend.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-cart',
  templateUrl: './my-cart.component.html',
  styleUrls: ['./my-cart.component.css']
})
export class MyCartComponent implements OnInit {
  list: any[] = [];
  myCart;

  constructor(private backendService: BackendService, private router: Router) { }

  ngOnInit(): void {
    this.backendService.getMyCart().subscribe(res => {
      this.myCart = res;
      this.list = res.products;
      //console.log(res); // {id: 52, products: Array(10), totalPrice: 21.999999999999996}
      //console.log(this.list); // 0:{id: 1, name: 'APPLE', price: 2.2} 1:{id: 1, name: 'APPLE', price: 2.2}
    });
  }

  delete(product: any): void {
    this.backendService.delete(product).subscribe(res => {
      this.ngOnInit();
    });

  }

  checkout(myCart: any): void {
    this.backendService.getUserDetail().subscribe(res => {
      if (res.address) {
        this.backendService.checkout(myCart).subscribe(res => {
          if (res.success) {
            this.router.navigate(['/myorders']);
          }
        });
      } else {
        this.router.navigate(['/userDetail']);
      }
    });
  }

}
